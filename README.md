Building ParaView
=================

1. prerequisites for below platform.
2. Build with  `time ./build-paraview.sh`
3. Run ParaView with `./run-paraview.sh`

Building SALOME
=================

1. Install development prerequisites for below platform.
2. If having gallium driver for display then use `./build-gallium.sh` and add your hostname into ./build-salome.sh
3. Build with	`time ./build-salome.sh`
4. Run SALOME with `source setupenv.sh`

Platform notes
--------------

### CentOS 7.2
 
The following packages are required on CentOS 7:

    yum -y groupinstall 'Development Tools'
    yum -y install redhat-lsb-core python-devel
    yum -y install libXt-devel libXmu-devel mesa-libGL-devel mesa-libGLU-devel
    yum -y install libXi-devel zlib-devel bzip2-devel libpng-devel 
    yum -y install libXrender-devel freeglut-devel texlive texlive-titlesec
    yum -y install texlive-framed texlive-threeparttable texlive-wrapfig
    yum -y install epel-release
    yum -y install texlive-upquote texlive-multirow

### Ubuntu 16.04

    time ./build-salome.sh

Required packages:

    apt-get install gfortran automake python-dev
    apt-get install bison flex freeglut3-dev git glut3-dev libbz2-dev
    apt-get install libgl1-mesa-dev libglu-dev libglut3-dev libpng16-dev
    apt-get install libtool libxi-dev libxm-dev libxmu-dev libxrender-dev
    apt-get install libxt-dev mesa-common-dev texlive zlib1g-dev 

### Debian 8.6

    apt-get install build-essential module-assistant make gcc g++ gfortran git
    m-a prepare
    apt-get install bison flex freeglut3-dev libbz2-dev libglw1-mesa libmotif-dev 
    apt-get install libgl1-mesa-dev libglu1-mesa-dev libpng12-dev libxi-dev 
    apt-get install libtool libxmu-dev libxrender-dev libxt-dev mesa-common-dev
    apt-get install texlive zlib1g-dev libncurses5-dev doxygen automake python-dev

### VirtualBox and Software OpenGL rendering platforms (cluster frontnodes)
    
ParaView 5.x requires OpenGL2 and GLSL 1.5 to render correctly! 
Many Linux platforms without hardware rendering are using incompatible Mesa3D
libraries. For that purpose `build-gallium.sh` script builds LLVM and MESA
libraries that are installed into staging directory, patched and used by 
PARAVIS module. Even then solid background is inclorrectly rendered transparent.
To fix that one can select *gradient background* permanently in ParaView
settings by selecting File -> Settings... -> ParaViS -> ParaView Settings -> 
Color Palette -> Use Gradient Background : Enable Gradient Background.


