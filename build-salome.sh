#!/bin/bash -x

SALOME_VERSION=${SALOME_VERSION:-8.2.0}
SALOME_PLATFORM=${SALOME_PLATFORM:-SRC}
SALOME_PRODUCT=SALOME_8_2_0


BUILDROOT=${PWD}
BUILD_DIR=${BUILDROOT}/build
PATCH_DIR=${BUILDROOT}/patches
DOWNLOAD_DIR=${BUILDROOT}/download
STAGING_DIR=${STAGING_DIR:-${BUILDROOT}/staging}
SALOME_DIR=${BUILD_DIR}/SALOME

echo "# setupenv.sh created by $0" > setupenv.sh

# Site specific defaults
# TODO Sites below needs to be upgraded to V8
case $(hostname -f) in
  *.iter.org) # RHEL5.11 with GCC 4.2
        #SALOME_PLATFORM=CO6.4
        module purge
        cat >> setupenv.sh <<EOF
module load GCC/4.8.3 binutils/2.25 autoconf/2.69
# The following preload is needed by ParaVis module to avoid crash
freetype_so=${SALOME_DIR}/PREREQUISITES/INSTALL/freetype-2.4.11/FROM_nothing/lib/libfreetype.so.6
test -e \${freetype_so} && export LD_PRELOAD=\${freetype_so}
EOF
        export echo=echo # needed by GMSHPLUGIN libtool that misses $echo
        ;;
  tp-t560-jp* \
  | *.marconi.cineca.it)
	. /etc/profile.d.gw/modules.sh
	module purge
	#SALOME_PLATFORM=CO7.2
	echo "export LIBGL_ALWAYS_INDIRECT=1" >>  setupenv.sh
	;;
esac

cat >> setupenv.sh <<EOF
# export CXX=g++ CC=gcc F77=gfortran
alias sat=${SALOME_DIR}/sat
alias salome=${SALOME_DIR}/salome
if test -f ${SALOME_DIR}/salomeTools/complete_sat.sh
  then . ${SALOME_DIR}/salomeTools/complete_sat.sh
fi
if test -f ${SALOME_DIR}/context.sh
  then . ${SALOME_DIR}/context.sh
fi
EOF

source setupenv.sh

set -e

## Initialize directories

install -d ${BUILD_DIR}
install -d ${DOWNLOAD_DIR}
install -d ${STAGING_DIR}

SALOME_PACKAGE_DIR=SALOME-${SALOME_VERSION}-${SALOME_PLATFORM}

SALOME_SRC=${SALOME_PACKAGE_DIR}.tgz
SALOME_SITE="http://salome-platform.org/downloads/current-version"
SALOME_DOWNLOAD="${SALOME_SITE}/DownloadDistr?platform=${SALOME_PLATFORM}&version=${SALOME_VERSION}"

if [ ! -f ${DOWNLOAD_DIR}/${SALOME_SRC} ]; then
    wget  -O ${DOWNLOAD_DIR}/${SALOME_SRC} ${SALOME_DOWNLOAD}
fi

cd ${BUILD_DIR}

if [ ! -d  ${SALOME_DIR} ]; then
    rm -rf ${SALOME_PACKAGE_DIR}
    tar xzf ${DOWNLOAD_DIR}/${SALOME_SRC}
    mv ${SALOME_PACKAGE_DIR} ${SALOME_DIR}
    DEBIAN_POOL=http://security.debian.org/debian-security/pool/updates/main
#    wget -O ${SALOME_DIR}/PREREQUISITES/SOURCES/freeimage-3.15.4.tar.gz \
#        ${DEBIAN_POOL}/f/freeimage/freeimage_3.15.4.orig.tar.gz
#    wget -O ${SALOME_DIR}/PREREQUISITES/SOURCES/lapack-3.5.0.tar.gz \
#         http://www.netlib.org/lapack/lapack-3.5.0.tgz
fi

if [ ! -e ${SALOME_DIR}/.patched ]; then
 # Remove local installation of Python 2.7 from extensions search path
 sed -i -e "/setup.py BUILD/a \sed -i -e 's|/usr/local|/nonexistent|g' setupext.py" \
     ${SALOME_DIR}/salomeTools/data/compil_scripts/prerequisites/matplotlib.sh

 case $(hostname -f) in
   *osboxes* | *.marconi.cineca.it )
	COPTS='CMAKE_OPTIONS=$CMAKE_OPTIONS" -D'
 	sed -i -e \
	'/VTK_RENDERING_BACKEND/{s/^#//;s/OpenGL2/OpenGL2/;s/D=/D:STRING=/}' \
	-e /VTK_RENDERING_BACKEND/a\
"export PKG_CONFIG_PATH=${STAGING_DIR}/mesa/lib/pkgconfig:\${PKG_CONFIG_PATH}\\n"\
"${COPTS}OPENGL_INCLUDE_DIR:PATH=${STAGING_DIR}/mesa/include\"\\n"\
"${COPTS}OPENGL_gl_LIBRARY:FILEPATH=${STAGING_DIR}/mesa/lib/libGL.so\"\\n"\
"${COPTS}CMAKE_EXE_LINKER_FLAGS:STRING=-L${STAGING_DIR}/llvm/lib\"\\n"\
"${COPTS}CMAKE_MODULE_LINKER_FLAGS:STRING=-L${STAGING_DIR}/llvm/lib\"\\n"\
          ${SALOME_DIR}/salomeTools/data/compil_scripts/prerequisites/ParaView-5.1.sh
	cd ${BUILDROOT}
	  ./build-gallium.sh
	;;
 esac

 touch ${SALOME_DIR}/.patched
fi


case $(hostname -f) in
   *osboxes* | *.marconi.cineca.it )
     echo "export LD_LIBRARY_PATH=${STAGING_DIR}/mesa/lib:\${LD_LIBRARY_PATH}"\
	  >> ${BUILDROOT}/setupenv.sh
     ;;
esac

cd ${SALOME_DIR}

if [ ! -e .prerequisites-deleted ]; then
    ./sat prerequisite ${SALOME_PRODUCT} --delete --force
    touch .prerequisites-deleted
fi

./sat prerequisite ${SALOME_PRODUCT}

if [ ! -e .modules-prepared ]; then
  if [ ${SALOME_PLATFORM} = CO6.4 ]
    then module load python/2.7/11
  fi
  ./sat prepare ${SALOME_PRODUCT}
#  patch -p 2 -d ${SALOME_DIR} < ${PATCH_DIR}/configure-check-qt5.patch
  touch .modules-prepared
fi

if [ ! -e .modules-cleaned ]; then
  touch .modules-cleaned
  ./sat compile ${SALOME_PRODUCT} --clean_all --force
else
  ./sat compile ${SALOME_PRODUCT}
fi

./sat launcher ${SALOME_PRODUCT}

sed -i -e "/workDir/s|'.*'|'${STAGING_DIR}'|" \
       -e "/devDir/s|'.*'|'${BUILDROOT}'|" \
       ${HOME}/.salomeTools/*.pyconf

# Prepare environment for sphinx-build generating documentation
echo 'echo "export SALOME_CONTEXT_ENVIRONMENT=\"SAMPLES_ROOT_DIR=${SAMPLES_ROOT_DIR} CONFIGURATION_ROOT_DIR=${CONFIGURATION_ROOT_DIR} MEDCOUPLING_ROOT_DIR=${MEDCOUPLING_ROOT_DIR} PATH=${PATH}:\${PATH} LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:\${LD_LIBRARY_PATH} PYTHONPATH=${PYTHONPATH}\""' | ./salome context | grep export > context.sh

touch .built
