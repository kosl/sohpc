#!/bin/sh -x
# Build Mesa3D Gallium driver for ParaView software OpenGL rendering

LLVM_VERSION=4.0.0
CMAKE_VERSION=3.7.2
MESA_VERSION=17.0.6

MAKE_JOBS=${MAKE_JOBS:-4}

BUILDROOT=${PWD}
BUILD_DIR=${BUILDROOT}/build
DOWNLOAD_DIR=${DOWNLOAD_DIR:-${BUILDROOT}/download}
STAGING_DIR=${STAGING_DIR:-${BUILDROOT}/staging}
#STAGING_DIR=${SWITMDIR}

#Initialize directories

install -d ${BUILD_DIR}
install -d ${DOWNLOAD_DIR}
install -d ${STAGING_DIR}


# We need recent CMAKE for building LLVM
CMAKE_TEST=$(hash cmake 2> /dev/null && cmake --version | sed 's/[^0-9]//g;s/^\(.\{2\}\).*/\1/')
if [ "${CMAKE_TEST}" -ge 35 ]
 then CMAKE=cmake
 else CMAKE=${STAGING_DIR}/bin/cmake
fi

set -e


# Install cmake as needed
CMAKE_SRC_DIR="${BUILD_DIR}/cmake-${CMAKE_VERSION}"
CMAKE_INSTALL_DIR="${STAGING_DIR}"
if [ ${CMAKE} != cmake -a  ! -e  ${CMAKE_SRC_DIR}/.built ]; then
  CMAKE_SRC="cmake-${CMAKE_VERSION}.tar.gz"
  CMAKE_MAIN_VERSION=${CMAKE_VERSION%.*}
  CMAKE_SITE="https://cmake.org/files/v${CMAKE_MAIN_VERSION}"
  CMAKE_DOWNLOAD="${CMAKE_SITE}/cmake-${CMAKE_VERSION}.tar.gz"
  if [ ! -f ${DOWNLOAD_DIR}/${CMAKE_SRC} ]; then
     wget  -O ${DOWNLOAD_DIR}/${CMAKE_SRC} --no-check-certificate \
	 ${CMAKE_DOWNLOAD}
  fi
  rm -rf ${CMAKE_SRC_DIR}
  cd ${BUILD_DIR}
  tar xzf ${DOWNLOAD_DIR}/${CMAKE_SRC}
  cd ${CMAKE_SRC_DIR}
  ./bootstrap --prefix=${STAGING_DIR}
  make -j ${MAKE_JOBS}
  make install
  touch ${CMAKE_SRC_DIR}/.built
fi


LLVM_SRC_DIR="${BUILD_DIR}/llvm-${LLVM_VERSION}.src"
LLVM_INSTALL_DIR="${STAGING_DIR}"

LLVM_SRC="llvm-${LLVM_VERSION}.src.tar.xz"
LLVM_DOWNLOAD="http://releases.llvm.org/${LLVM_VERSION}/${LLVM_SRC}"

if [ ! -e  ${LLVM_SRC_DIR}/.built ]; then
  if [ ! -f ${DOWNLOAD_DIR}/${LLVM_SRC} ]; then
     wget  -O ${DOWNLOAD_DIR}/${LLVM_SRC} --no-check-certificate \
	 ${LLVM_DOWNLOAD}
  fi
  rm -rf ${LLVM_SRC_DIR}
  cd ${BUILD_DIR}
  tar xJf ${DOWNLOAD_DIR}/${LLVM_SRC}
  cd ${LLVM_SRC_DIR}
  mkdir BUILD
  cd BUILD
  ${CMAKE}  -DCMAKE_BUILD_TYPE=Release              \
      -DCMAKE_INSTALL_PREFIX=${STAGING_DIR}/llvm    \
      -DLLVM_BUILD_LLVM_DYLIB=ON                    \
      -DLLVM_ENABLE_RTTI=ON                         \
      -DLLVM_INSTALL_UTILS=ON                       \
      -DLLVM_TARGETS_TO_BUILD:STRING=X86            \
      ..
  make -j ${MAKE_JOBS}
  make install
  touch ${LLVM_SRC_DIR}/.built
fi

MESA_SRC="mesa-${MESA_VERSION}.tar.xz"
MESA_SRC_DIR="${BUILD_DIR}/mesa-${MESA_VERSION}"
MESA_DOWNLOAD="https://mesa.freedesktop.org/archive/${MESA_SRC}"
if [ ! -e  ${MESA_SRC_DIR}/.built ]; then
  if [ ! -f ${DOWNLOAD_DIR}/${MESA_SRC} ]; then
     wget -O ${DOWNLOAD_DIR}/${MESA_SRC} --no-check-certificate \
	 ${MESA_DOWNLOAD}
  fi
  rm -rf ${MESA_SRC_DIR}
  cd ${BUILD_DIR}
  tar xJf ${DOWNLOAD_DIR}/${MESA_SRC}
  cd ${MESA_SRC_DIR}
  export PATH=${STAGING_DIR}/llvm/bin:${PATH}
  LDFLAGS="-Wl,-rpath -Wl,${STAGING_DIR}/llvm/lib"
  LDFLAGS="${LDFLAGS} -Wl,-rpath-link,${STAGING_DIR}/llvm/lib"  \
  ./configure                                           \
      --prefix=${STAGING_DIR}/mesa                      \
      --enable-opengl --disable-gles1 --disable-gles2   \
      --disable-va --disable-xvmc --disable-vdpau       \
      --enable-shared-glapi                             \
      --disable-texture-float                           \
      --enable-gallium-llvm --enable-llvm-shared-libs   \
      --with-gallium-drivers=swrast,swr                 \
      --disable-dri --with-dri-drivers=                 \
      --disable-egl --with-egl-platforms= --disable-gbm \
      --disable-osmesa --enable-gallium-osmesa --enable-glx
  make -j ${MAKE_JOBS}
  make install
  touch ${MESA_SRC_DIR}/.built
fi
